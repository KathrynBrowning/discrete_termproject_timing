﻿using System;

namespace KBrowning_TermProject_Timing
{
    /// <summary>
    ///     Controls the timing of the 1000 different arrays
    /// </summary>
    public class ArrayTiming
    {
        public TimeSpan TotalTime { get; set; }
        public TimeSpan AverageRunningTime { get; set; }

        private const int OneThousand = 1000;

        private DateTime startTime;
        private DateTime endTime;

        /// <summary>
        /// Loops 1000 times and gets the average running time of  the sorting of each array size.
        /// </summary>
        /// <param name="arraySize">Size of the array.</param>
        public void Loop(int arraySize)
        {
             for (var i = 0; i < OneThousand; i++)
            {
                var normalArray = new NormalArray();
                var array = normalArray.GenerateArrayWithRandomNumber(arraySize);

                this.startTime = DateTime.Now;

                var bubbleArray = new BubbleSortArray();
                bubbleArray.BubbleSortedArray(array);            

                this.endTime = DateTime.Now;
                var runningTime = this.endTime - this.startTime;
                this.TotalTime += runningTime;
            }
            this.setAverageRunningTime();
        }

        private void setAverageRunningTime()
        {
            this.AverageRunningTime = new TimeSpan(this.TotalTime.Ticks / 1000);
        }
    }
}