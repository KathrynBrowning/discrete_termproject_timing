﻿namespace KBrowning_TermProject_Timing
{
    /// <summary>
    ///     This class computes the Bubble Sort Array.
    /// </summary>
    public class BubbleSortArray
    {
        /// <summary>
        ///     Sorts the array with a bubble sorting algorithm.
        /// </summary>
        /// <param name="array"> The normal array .</param>
        /// <precondition> array != null </precondition>
        /// <postcondition> Array has been sorted. </postcondition>
        /// <returns> The sorted array. </returns>
        public int[] BubbleSortedArray(int[] array)
        {
            for (var i = 0; i < array.Length; i++)
            {
                for (var j = 0; j < array.Length - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        var temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
            return array;
        }
    }
}
