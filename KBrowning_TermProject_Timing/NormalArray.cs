﻿using System;
using System.Linq;


namespace KBrowning_TermProject_Timing
{
    /// <summary>
    ///     Creates a normal array.
    /// </summary>
    public class NormalArray
    {

        /// <summary>
        ///     Generates the array with random number.
        /// </summary>
        /// <param name="userSelectedNumber">The user selected number.</param>
        /// <returns> A normal, unsorted array. </returns>
        public int[] GenerateArrayWithRandomNumber(int userSelectedNumber)
        {
            var randomNumber = new Random();
            var normalArray = Enumerable.Repeat(0, userSelectedNumber).Select(i => randomNumber.Next()).ToArray();
            return normalArray;
        }
    }
}
