﻿
namespace KBrowning_TermProject_Timing
{
    /// <summary>
    ///     Starting point of the program.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Starting point of the program.
        /// </summary>
        /// <param name="args">Not used.</param>
        private static void Main(string[] args)
        {
            var userInterface = new UI();
            userInterface.Run();
        }
    }
}
