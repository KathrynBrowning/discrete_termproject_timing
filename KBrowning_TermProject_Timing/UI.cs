﻿using System;

namespace KBrowning_TermProject_Timing
{
    /// <summary>
    ///     Controls the display and user interaction.
    /// </summary>
    public class UI
    {
        private ArrayTiming arrayTiming;
        private int enteredNumber;

        private const int OneThousand = 1000;
        private const int FiveHundred = 500;
        private const int TwoFifty = 250;
        private const int Fifty = 50;

        /// <summary>
        ///     Runs this instance.
        /// </summary>
        public void Run()
        {
            this.initialInput();
            this.displayTimingWithUserEnteredSize();
            this.displayTimingWithSize500();
            this.displayTimingWithSize250();
            this.displayTimingWithSize50();
            Console.ReadLine();
        }

        private void initialInput()
        {
            while (true)
            {
                Console.WriteLine("\nPlease enter the size you'd like the array to be:");
                this.enteredNumber = Convert.ToInt32(Console.ReadLine());
                if (this.enteredNumber >= 1)
                {
                    return;
                }
                Console.Write("Please enter a positive number greater than 0.\nPress a key to restart.\n");
                Console.ReadLine();
            }
        }

        private void displayTimingWithUserEnteredSize()
        {
            this.arrayTiming = new ArrayTiming();
            Console.WriteLine("\nGenerating 1000 arrays of size " + this.enteredNumber + "...");
            this.arrayTiming.Loop(this.enteredNumber);
            this.displaySummary();
            Console.WriteLine("\nPress any key to see this with an array of size 500: ");
            Console.ReadLine();
        }

        private void displayTimingWithSize500()
        {
            this.arrayTiming = new ArrayTiming();
            Console.WriteLine("\nGenerating 1000 arrays of size " + FiveHundred + "...");
            this.arrayTiming.Loop(FiveHundred);
            this.displaySummary();
            Console.WriteLine("\nPress any key to see this with an array of size 250: ");
            Console.ReadLine();
        }

        private void displayTimingWithSize250()
        {
            this.arrayTiming = new ArrayTiming();
            Console.WriteLine("\nGenerating 1000 arrays of size " + TwoFifty + "...");
            this.arrayTiming.Loop(TwoFifty);
            this.displaySummary();
            Console.WriteLine("\nPress any key to see this with an array of size 50: ");
            Console.ReadLine();
        }

        private void displayTimingWithSize50()
        {
            this.arrayTiming = new ArrayTiming();
            Console.WriteLine("\nGenerating 1000 arrays of size " + Fifty + "...");
            this.arrayTiming.Loop(Fifty);
            this.displaySummary();
            Console.WriteLine("Program Complete");
            Console.ReadLine();
        }

        private void displaySummary()
        {
            Console.WriteLine("Total items sorted: " + OneThousand + "\nAverage running time: " + this.arrayTiming.AverageRunningTime);
        }
    }
}
